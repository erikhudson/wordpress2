# Install gitlab-runner via helm v3

# Adicionando o Repositorio
helm repo add gitlab https://charts.gitlab.io

helm repo update

# Install For Helm 3
helm upgrade --install --namespace gitlab-runner --create-namespace gitlab-runner -f  runner-chart-values.yaml gitlab/gitlab-runner